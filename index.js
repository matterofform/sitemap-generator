/*!
 * sitemap-generator
 * Copyright(c) 2019 Matter of Form
 * Authors: Paul Masek and Chris Wilcox
 * ITC Licensed
 */

"use strict";

module.exports = require("./src/index");
