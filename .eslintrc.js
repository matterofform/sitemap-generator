module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  // add your custom rules here
  rules: {
    'prettier/prettier': 'error',
    'max-len': 'off',
    'no-underscore-dangle': 'off',
    'arrow-body-style': 'off',
    'arrow-parens': ['error', 'as-needed'],
    'import/no-extraneous-dependencies': 0,
    'import/no-unresolved': 0,
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
};
