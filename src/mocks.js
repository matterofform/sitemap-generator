const fs = require('fs');
const util = require('util');
const path = require('path');

const MOCK_PATH = path.resolve(__dirname, './mocks/update.json');
const write = util.promisify(fs.writeFile);
const read = util.promisify(fs.readFile);

function createMock(update) {
  return write(MOCK_PATH, JSON.stringify(update));
}

module.exports.createMock = createMock;

async function getMock() {
  return new Promise(async (resolve, reject) => {
    const data = await read(MOCK_PATH)
      .then(fileData => {
        const jsonObj = JSON.parse(fileData);
        resolve(jsonObj);
      })
      .catch(error => reject(error));

    return data;
  });
}

module.exports.getMock = getMock;
