const fs = require('fs');
const mkdirp = require('mkdirp');

function outputToFile(config) {
  return sitemaps => {
    let { path = '' } = config;
    sitemaps.forEach(sitemapObj => {
      const outputPath = `${path}/${sitemapObj.filename}`;
      mkdirp.sync(path); // make output directoty if it doesn't exist
      fs.writeFileSync(outputPath, sitemapObj.sitemap.toString());
    });
  };
}

module.exports = outputToFile;
