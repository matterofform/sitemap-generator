// upload files to s3.
// requires environmental variables
// S3_BUCKET_NAME: s3 bucket name
// DEPLOY_STAGE: (optional) directory path used in s3 bucket (ie dev)

const aws = require('aws-sdk');

const S3 = new aws.S3();

function uploadToS3(config) {
  return async sitemaps => {
    sitemaps.forEach(sitemapObj => {
      const params = {
        Bucket: process.env.S3_BUCKET_NAME,
        Key: `${process.env.DEPLOY_STAGE}/${sitemapObj.filename}`,
        ContentType: 'application/xml',
        Body: sitemapObj.sitemap.toString(),
      };
      S3.putObject(params)
        .promise()
        .then(data => console.log(`File ${params.Key} uploaded`, data), error => console.error(error));
    });
  };
}

module.exports = uploadToS3;
