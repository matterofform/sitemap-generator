const sitemapLib = require('sitemap');
const kebabCase = require('lodash.kebabcase');

const PAGE = 'PAGE';
const POST = 'POST';

function shouldBuild(func, requestData, templates) {
  return func(requestData, templates);
}

function getEntries(func) {
  return func(getEntries);
}

function mergeTypes(current = [], entry) {
  return [
    ...current,
    {
      ...entry,
    },
  ];
}

function processUrlSlug(id, slug, prependSlug, homeId) {
  if (id === homeId) {
    return '/';
  }

  if (prependSlug) {
    return `/${prependSlug}${slug}/`;
  }

  return `/${slug}/`;
}

function processEntry(entry, templateObj, homeId) {
  return {
    ...entry,
    url: processUrlSlug(entry.id, entry.slug, templateObj.prependSlug, homeId),
  };
}

function getKey(templates, contentType) {
  const templateObj = templates.find(template => template.contentType === contentType);

  if (templateObj) {
    return kebabCase(templateObj.key);
  }

  return contentType;
}

function processEntries(entries, templates, homeId) {
  let collection = [];

  const processed = entries.reduce((store, entry) => {
    const templateObj = templates.find(template => template.contentType === entry.contentType);

    if (templateObj) {
      const { contentType } = entry;
      const processedEntry = processEntry(entry, templateObj, homeId);
      const { type, archiveFor } = templateObj;
      let key = getKey(templates, contentType);

      collection = [...collection, processedEntry];

      if (type === PAGE) {
        let returnStore = { ...store };

        if (archiveFor) {
          key = getKey(templates, archiveFor);

          returnStore = {
            ...store,
            [key]: mergeTypes(store[key], processedEntry),
          };
        }

        return {
          ...returnStore,
          page: mergeTypes(store.page, processedEntry),
        };
      }
      if (type === POST) {
        return {
          ...store,
          [key]: mergeTypes(store[key], processedEntry),
        };
      }
    }

    console.info('Entry content type not found in templates', entry);

    return store;
  }, {});

  return {
    collection,
    processed,
  };
}

function buildSitemapIndex(baseUrl, sitemapCollection, xsl) {
  const indexUrls = sitemapCollection.map(sitemapObj => {
    return `${baseUrl}/${sitemapObj.filename}`;
  });

  const indexSitemap = sitemapLib.buildSitemapIndex({
    urls: indexUrls,
    xslUrl: xsl,
  });

  return {
    type: 'index',
    filename: 'sitemap-index.xml',
    sitemap: indexSitemap,
  };
}

function generateSitemaps(baseUrl, collection, xsl) {
  const sitemaps = Object.keys(collection).map(key => {
    const type = collection[key];

    const sitemap = sitemapLib.createSitemap({
      hostname: baseUrl,
      urls: type,
      xslUrl: xsl,
    });

    return {
      type: key,
      filename: `${key}-sitemap.xml`,
      sitemap,
    };
  });

  const indexSitemap = buildSitemapIndex(baseUrl, sitemaps, xsl);

  return [...sitemaps, indexSitemap];
}

async function buildSitemap(baseUrl, requestData, templates, adapter, outputHandler, homeId, xsl) {
  return new Promise(async (resolve, reject) => {
    try {
      const shouldRebuild = shouldBuild(adapter.shouldBuild, requestData, templates);

      if (shouldRebuild.length > 0) {
        console.info('One of following templates has changed', shouldRebuild);

        const entries = await getEntries(adapter.getEntries);
        const { collection, processed } = processEntries(entries, templates, homeId);
        const sitemaps = await generateSitemaps(baseUrl, processed, xsl);

        await outputHandler(sitemaps);

        resolve({
          message: 'Sitemap built - pages directly updated',
          built: true,
          sitemaps,
        });
      }
      resolve({
        message: 'Sitemap not built - update not to pages',
        built: false,
      });
    } catch (e) {
      reject(e);
    }
  });
}

module.exports.buildSitemap = buildSitemap;
