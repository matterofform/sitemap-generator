// verify if required value is set
// optionally also check for typeof
// @required: object of values to test, if array will test for defined and typeof
//
// example
//
// required = {
//  foo: value1,
//  bar: [value2, 'function'],
//  foo2: [value3, 'array'],
// }
function verifyRequired(required) {
  return Object.keys(required).reduce((result, key) => {
    let passed = true;
    if (Array.isArray(required[key])) {
      passed =
        verifyValueEqual(typeof required[key][0], 'undefined', key, 'defined') &&
        verifyValueNotEqual(typeof required[key][0], required[key][1], key, `not a ${required[key][1]}`);
    } else {
      passed = verifyValueEqual(typeof required[key], 'undefined', key, 'defined');
    }

    return result ? passed : false;
  }, true);
}

function verifyValueEqual(testValue, target, key, error) {
  if (testValue === target) {
    logError(key, error);
    return false;
  }
  return true;
}

function verifyValueNotEqual(testValue, target, key, error) {
  if (testValue !== target) {
    logError(key, error);
    return false;
  }
  return true;
}

function logError(key, message) {
  console.error(`SITEMAP-GENERATOR ERROR\nRequired value ${key} not ${message}\n**********`);
}
module.exports.verifyRequired = verifyRequired;
