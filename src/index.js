const express = require('express');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const { validator } = require('./auth.js');
const { handleContentUpdate } = require('./handle-content-update.js');
const { verifyAdapter } = require('./verify-adapter.js');
const { verifyRequired } = require('./verify-required.js');

dotenv.config();

function sitemap(config) {
  // check required env values
  if (
    !verifyRequired({
      NODE_ENV: process.env.NODE_ENV,
      URL: process.env.URL,
    })
  )
    exitProcess('required environment variables not defined.');

  return startSitemapApp(config);
}

function startSitemapApp(config) {
  const { URL } = process.env;
  const app = express();
  const port = 9999;
  const updateSitemapPath = process.env.UPDATE_SITEMAP_ROUTE ? process.env.UPDATE_SITEMAP_ROUTE : '/sitemap/update';
  const outputHandler = require(`./output-handler/${config.output.handler}`)(config.output);

  // check adapter for required config values
  const verifiedAdapter = verifyAdapter(config.adapter);

  // check required values
  if (
    verifiedAdapter &&
    verifyRequired({
      URL: URL,
      updateSitemapPath: updateSitemapPath,
      templates: [config.templates, 'object'],
      outputHandler: [outputHandler, 'function'],
      homeId: config.homeId,
    })
  ) {
    app.locals.config = {
      ...config,
      outputHandler: outputHandler,
      url: URL,
    };
  } else {
    console.info('Failed to start app, exiting');
    process.exit(1);
  }

  // Raw body needed for for auth
  app.use(
    bodyParser.json({
      verify(req, res, buf) {
        req.rawBody = buf;
      },
    })
  );

  app.use(
    bodyParser.urlencoded({
      extended: false,
      verify(req, res, buf) {
        req.rawBody = buf;
      },
    })
  );

  if (process.env.NODE_ENV === 'dev') {
    app.listen(port, async () => {
      console.log(`Sitemap generator running on port ${port}!`);
    });
  }

  app.post(updateSitemapPath, validator, handleContentUpdate);

  return app;
}

function exitProcess(message) {
  console.info('Failed to start app, exiting', message);
  process.exit(1);
}

module.exports = sitemap;
