const { DeliveryClient } = require('kentico-cloud-delivery');
const crypto = require('crypto');

const { LANGUAGE, KENTICO_PROJECT_ID } = process.env;

function validateRequest(body, headers, secret) {
  const signature = headers['x-kc-signature'];

  if (body && signature && secret) {
    const computedSignature = crypto
      .createHmac('sha256', secret)
      .update(body)
      .digest();

    const check = crypto.timingSafeEqual(Buffer.from(signature, 'base64'), computedSignature);

    return check;
  }

  return false;
}

function shouldBuild(requestData, templates) {
  const { data } = requestData;

  if (data && data.items && data.items.length > 0) {
    const templatesFound = data.items.reduce((store, item) => {
      const templateObj = templates.find(template => template.contentType === item.type);

      if (templateObj) {
        return [
          ...store,
          {
            ...templateObj,
          },
        ];
      }

      return store;
    }, []);

    return templatesFound;
  }

  return [];
}

function setupClient(projectId) {
  return new DeliveryClient({
    projectId,
  });
}

function responseProcessor(response) {
  const { items } = response;

  if (items && items.length > 0) {
    const processed = items.reduce((store, item) => {
      const { system: itemInfo, elements } = item;

      if (itemInfo && elements) {
        const { type: contentType, lastModified: lastmodISO, name, codename } = itemInfo;
        const { slug } = elements;

        if (contentType && lastmodISO && name && slug && slug.value) {
          return [
            ...store,
            {
              id: codename,
              slug: slug.value,
              contentType,
              lastmodISO,
            },
          ];
        }
      }

      return store;
    }, []);

    return processed;
  }

  return [];
}

async function getEntries() {
  return new Promise((resolve, reject) => {
    const client = setupClient(KENTICO_PROJECT_ID);
    const request = client
      .items()
      .languageParameter(LANGUAGE)
      .elementsParameter(['slug'])
      .greaterThanOrEqualFilter('elements.slug', 0)
      .depthParameter(1);

    request
      .getPromise()
      .then(response => {
        const processed = responseProcessor(response);
        resolve(processed);
      })
      .catch(error => reject(error));
  });
}

module.exports = {
  name: 'Kentico cloud sitemap generator',
  validateRequest,
  shouldBuild,
  getEntries,
};
