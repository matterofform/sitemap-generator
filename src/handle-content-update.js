const { buildSitemap } = require('./sitemap-generator.js');

async function handleContentUpdate(req, res) {
  try {
    const { templates, adapter, url, outputHandler, homeId, xsl } = req.app.locals.config;
    const update = req.body;

    const updatedSitemap = await buildSitemap(url, update, templates, adapter, outputHandler, homeId, xsl);

    if (updatedSitemap.built) {
      return res.json({
        message: 'Sitemap rebuilt',
      });
    }

    return res.json({
      message: 'Sitemap not built - no pages directly updated',
    });
  } catch (e) {
    console.error(e);

    return res.status(500).json({
      message: 'Error building sitemap - check logs',
    });
  }
}

module.exports.handleContentUpdate = handleContentUpdate;
