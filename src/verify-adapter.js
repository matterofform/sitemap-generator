function verifyAdapter(adapter) {
  const requiredFunctions = ['validateRequest', 'shouldBuild', 'getEntries'];
  const passed = requiredFunctions.reduce((result, func) => {
    if (typeof adapter[func] === 'undefined') {
      console.info(`${func} function missing from Adapter`);
      return false;
    }

    return true;
  }, null);

  return passed;
}

module.exports.verifyAdapter = verifyAdapter;
