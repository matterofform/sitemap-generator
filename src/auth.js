// in production default to authentication
// unless WEBHOOK_SUPPRESS_AUTH = 'true' is defined
// otherwise default to no authentication.

const AUTH = process.env.WEBHOOK_SUPPRESS_AUTH ? false : process.env.NODE_ENV === `production`;

function validator(req, res, next) {
  if (AUTH) {
    const { adapter } = req.app.locals.config;
    const isValid = adapter.validateRequest(req.rawBody, req.headers, process.env.SITEMAP_SECRET);

    if (isValid) {
      return next();
    }

    return res.status(409).json({
      error: 'Invalid signature',
    });
  }

  return next();
}

module.exports.validator = validator;
