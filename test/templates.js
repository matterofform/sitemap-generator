const PAGE = 'PAGE';
const POST = 'POST';

const templates = [
  {
    name: 'Homepage',
    key: 'homepage',
    contentType: 'template___homepage',
    type: PAGE,
  },
  {
    name: 'The Blog',
    key: 'blog',
    contentType: 'template___blog',
    type: PAGE,
    archiveFor: 'template___blog_post',
  },
  {
    name: 'Blog Post',
    key: 'blog_post',
    prependSlug: 'blog/',
    contentType: 'template___blog_post',
    type: POST,
  },
];

module.exports = templates;
