const templates = require('./templates.js');
const sitemap = require('../index.js');
const adapter = require('@matterofform/sitemap-adapter-kenticocloud');

const app = sitemap({
  adapter,
  templates,
  output: {
    handler: 's3-uploader',
    handler: 'save-to-file',
    path: 'dist',
  },
  homeId: 'homepage',
  xsl: '/sitemap.xsl',
});

module.exports = app;
