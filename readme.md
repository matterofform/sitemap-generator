# Sitemap Generator
Output sitemap to file or s3 from a headless CMS source. It is intended to be triggered by a webhook notification that content has changed. To connect to a datasource you will also need an adapter. We have written one for Kentico Cloud.

**NB. This is in early development so not fully tested in all scenarios, Use at your own peril**

[sitemap-adapter-kenticocloud](https://www.npmjs.com/package/@matterofform/sitemap-adapter-kenticocloud)


## Installation

```js
npm install @matterofform/sitemap-generator @matterofform/sitemap-adapter-kenticocloud

yarn add @matterofform/sitemap-generator @matterofform/sitemap-adapter-kenticocloud
```

## Requirements
### Environment variables
```js
NODE_ENV // node environment ie dev, production
S3_BUCKET_NAME // bucketname for uploading to S3 (optional)
S3_DOMAIN= // bucketname for uploading to S3 (optional)
URL // URL to use for sitemap
UPDATE_SITEMAP_ROUTE // route to use for update notifications; defaults to `/sitemap/update`
LANGUAGE // optional - used by sitemap-adapter-kenticocloud
```
## Usage

Sitemap generator requires an array of templates to verify against the response from the adapter to see whether to rebuild the sitemap. For example refer to `/test` folder

### upload to s3
```js
const sitemap = require('@matterofform/sitemap-generator');
const adapter = require('@matterofform/sitemap-adapter-kenticocloud');
const templates = require('./templates.js');

const app = sitemap({
  adapter,
  templates,
  output: {
    handler: 's3-uploader'
  },
  homeId: 'homepage',
  xsl: '/sitemap.xsl'
});
```

### output to file
```js
const sitemap = require('@matterofform/sitemap-generator');
const adapter = require('@matterofform/sitemap-adapter-kenticocloud');
const templates = require('./templates.js');

const app = sitemap({
  adapter,
  templates,
  output: {
    handler: 'save-to-file',
    path: 'dist'
  },
  homeId: 'homepage',
  xsl: '/sitemap.xsl'
});
```
